FROM node:17-alpine

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY index.html .
COPY server.js .

ENV PRODUCTS_SERVICE="productsfirst"
ENV SHOPPING_CART_SERVICE="shoppingcartfirst"

EXPOSE 3000
CMD ["npm", "start"]
